$(function () {
    // CLICKING A CARD
    $('.card').click(function () {
        $(this).addClass('selected-card'); // Distribute a class to the selected card  
        $('.card').not('.' + 'selected-card').addClass('fadeOut'); // Target and fadeout all cards but the clicked one

        $(this).addClass('turnScaleUp-card'); // Transforms the card with scale and rotate
        
        $(this).children('.front').fadeOut(300); // IE fix to remove the content of the cards front
        
        // Moves the selected card to the center
        $(this).animate({
            left: calculateHorizontalCardDestination($(this), 'forward'),
            top: calculateVerticalCardDestination($(this), 'forward')
        }, 100);

        $('.btn-reveal').addClass('show'); // Revealing the "reveal-button"
        $('.btn-reveal').show(); // This clears possible earlier hides
    });
    
    // REVEALING A CARD
    $('.btn-reveal').click(function () {
        $(this).hide(); // Hide the reveal-button  
        $('.selected-card').children('.front').fadeIn(300); // IE fix to show the content of the cards front
        $('.selected-card').removeClass('turnScaleUp-card'); // Removes the scale
        $('.selected-card').addClass('reveal-card'); // Rotate card

        $('.btn-reveal').removeClass('show'); // Clear button from show class
        $('.btn-again').addClass('show'); // Revealing the "again-button"
        $('.btn-again').show(); // This clears possible earlier hides
    });
    
    // PLAY AGAIN    
    $('.btn-again').click(function () {
        $(this).hide(); // Hide the again-button
        $('.btn-again').removeClass('show'); // Clear button from show class
        $('.card').not('.' + 'selected-card').removeClass('fadeOut'); // Fade in all cards
        
        $('.selected-card').removeClass('turnScaleUp-card, reveal-card'); // Clear cards from transforms
        
        $('.card').addClass('ScaleDown-card'); // Scale down the selected card

        // Moves the selected card back to its origin position
        $('.selected-card').animate({
            left: calculateHorizontalCardDestination($(this), 'back'),
            top: calculateVerticalCardDestination($(this), 'back')
        }, 100);

        $('.card').removeClass('selected-card'); // Release the card from the selection
        $('.card').removeClass('ScaleDown-card'); // Clear cards from transforms
    });

});

// Calculate horizontal distance for animation depending on movement (forward or back)
function calculateHorizontalCardDestination(element, movement) {
    var parentLeft = element.parent().offset().left; //Establishing the left position of the cards container
    var parentHalfWidth = element.parent().width() / 2; // Calculate half of cards container width
    var parentCenter = parentLeft + parentHalfWidth; // Calculate the center of the container by adding left position and half of the conatiner width

    var thisLeft = element.offset().left; //Establishing the left position of the card
    var thisHalfWidth = element.width() / 2; // Calculate half of the cards width

    return movement === 'forward' ? (parentCenter - thisLeft) - thisHalfWidth : 0; // If the card is selected give it the calculated position. If the card is released from the selection, give it position 0, which is its origin position. 
};

// Calculate vertical distance for animation depending on movement (forward or back)
function calculateVerticalCardDestination(element, movement) {
    var parentTop = element.parent().offset().top; //Establishing the top position of the cards container
    var parentHalfHeight = element.parent().height() / 2; // Calculate half of cards container height
    var parentCenter = parentTop + parentHalfHeight; // Calculate the center of the container by adding top position and half of the conatiner height

    var thisTop = element.offset().top; //Establishing the top position of the card
    var thisHalfHeight = element.height() / 2; // Calculate half of the cards height

    return movement === 'forward' ? (parentCenter - thisTop) - thisHalfHeight : 0; // If the card is selected give it the calculated position. If the card is released from the selection, give it position 0, which is its origin position.
};